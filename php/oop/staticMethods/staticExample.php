<?php
/**
 * File contains examples of using static methods and properties.
 */

/**
 * Example of class with static methods and properties.
 */
class StaticExample
{
    static public $aNum = 0;

    static public function sayHello()
    {
        self::$aNum++;
        echo 'Hello! $aNum = ' . self::$aNum;
    }
}


echo StaticExample::$aNum . "<br>";
StaticExample::sayHello();