<?php
/**
 * File contains examples of using abstract class.
 */

/**
 * Example of abstract class.
 */
abstract class AbstractClass
{
    /**
     * @var mixed $publicProperty Example of public propperty.
     * @var string $protectedProperty Example of protected propperty.
     * @var mixed $privateProperty This propperty can't be accessible.
     */
    public $publicProperty;
    protected $protectedProperty;
    private $privateProperty = 'Hello!<br>';

    /**
     * Abstract class constructor.
     */
    function __construct()
    {
        echo 'Initialized from ' . __METHOD__ . '<br>';
    }

    /**
     * Declare abstract public function.
     *
     * @return void
     */
    abstract public function abstractPublicFunction($a);

    /**
     * Declare abstract protected function.
     *
     * @return void
     */
    abstract protected function abstractProtectedFunction();

    /**
     * Public static function example.
     *
     * @return void
     */
    public static function publicCommonFunction()
    {
        echo 'Output from ' . __METHOD__ . '<br>';
    }

    /**
     * Set property $protectedProperty value.
     *
     * @param string|null $value Incoming string.
     */
    public function setProtectedProperty($value = null)
    {
        $this->protectedProperty = $value;
    }

    /**
     * Get property $protectedProperty value.
     *
     * @return string|null The $protectedProperty value.
     */
    public function getProtectedProperty()
    {
        return $this->protectedProperty;
    }
}


/**
 * Example of extending abstract class.
 */
class StandardClass extends AbstractClass
{
    /**
     * Redeclare constructor for StandardClass.
     */
    function __construct()
    {
        parent::__construct();
        echo 'Initialized from ' . __METHOD__ . '<br>';
    }

    /**
     * Realize function declared in AbstractClass.
     *
     * @return void
     */
    public function abstractPublicFunction($a) {
        $this->abstractProtectedFunction();

        return $a . ' - Output from ' . __METHOD__ . '<br>';
    }

    /**
     * Realize function declared in AbstractClass.
     *
     * @return void
     */
    protected function abstractProtectedFunction() {
        echo 'Output from ' . __METHOD__ . '<br>';
    }
}



AbstractClass::publicCommonFunction();
StandardClass::publicCommonFunction();
$obg = new StandardClass();

echo $obg->abstractPublicFunction('Hello!');
$obg->setProtectedProperty('string');
echo $obg->getProtectedProperty();
$obg->publicProperty;

echo '<pre>';
var_dump($obg);