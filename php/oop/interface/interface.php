<?php
/**
 * File contains examples of using interfaces.
 */

/**
 * Example of interface.
 */
interface InterfaceExample
{
    public function setDate($date);
    public function printDate();
}

/**
 * Example of interface constant.
 */
interface InterfaceExample2
{
    const CONST_EXAMPLE = 'I am CONSTANT from InterfaceExample2';
}

/**
 * Example of interface inheritance.
 */
interface InterfaceExample3 extends InterfaceExample, InterfaceExample2
{
    public function iterate();
    public function getIterator();
}

/**
 * Example of class implements interface.
 */
class ClassExample implements InterfaceExample3
{
    private $date;
    private $i = 0;

    function __construct()
    {
        echo self::CONST_EXAMPLE . '<br>';
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function printDate()
    {
        echo $this->date;
    }

    public function iterate()
    {
        $this->i++;
    }

    public function getIterator()
    {
        return $this->i;
    }
}


$obj = new ClassExample();
$obj->setDate('01.01.17');
$obj->printDate();
echo '<br>';
$obj->iterate();
$obj->iterate();
echo $obj->getIterator() . '<br>';

//
echo InterfaceExample2::CONST_EXAMPLE . '<br>';