<?php
/**
 * File contains examples of using trates.
 */

/**
 * Trait example.
 */
trait Trait1
{
    public function sayHello()
    {
        echo 'Buongiorno from ' . __METHOD__ . '<br>';
    }
}

/**
 * Trait example.
 */
trait Trait2
{
    public function sayHello()
    {
        echo 'Hi from ' . __METHOD__ . '<br>';
    }
}

/**
 * Trait example.
 */
trait Trait3
{
    public function sayGoodby()
    {
        echo 'Goodby from ' . __METHOD__ . '<br>';
    }
}

/**
 * Example of class using traits.
 */
class ClassExample
{
    use Trait1, Trait2, Trait3 {
        Trait1::sayHello insteadof Trait2;
        Trait1::sayHello as sayBuongiorno;
        Trait2::sayHello as sayHi;
        sayGoodby as protected;
    }

    public function sayHello()
    {
        echo 'Hello from ' . __METHOD__ . '<br>';
    }

    public function goodby()
    {
        $this->sayGoodby();
    }
}

$obj = new ClassExample();
$obj->sayHello();
$obj->sayBuongiorno();
$obj->sayHi();
$obj->goodby();