<?php
/**
 * File contains examples of using namespaces.
 */

namespace Namespace1;

include 'space2/namespace2.php';
use Namespace1\Namespace2;

include 'space3/namespace3.php';
use Namespace3;

include 'space4/namespace4.php';
use www\eee\Namespace4, qqq\aaa\Namespace4 as Namespace5;

/**
 * Simpe CONST.
 */
const CONST_EXAMPLE = 1;

/**
 * Simpe class.
 */
class ClassExample {
    public static function staticMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
    public function simpleMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
}

/**
 * Simpe function.
 */
function functionExample()
{
    echo __FILE__ . '***' . __FUNCTION__ . '<br>';
}


// Global space.
$obj = new \Namespace1\ClassExample;

$obj2 = new namespace\ClassExample;

$const = namespace\CONST_EXAMPLE;
$const2 = __NAMESPACE__ . '\CONST_EXAMPLE';

echo '<pre>';
var_dump($obj, $obj2, __NAMESPACE__, $const, constant($const2));

namespace\functionExample();
namespace\ClassExample::staticMethod();

// Included namespace2.
Namespace2\ClassExample::staticMethod();
// OR
\Namespace1\Namespace2\ClassExample::staticMethod();

// Included namespace3.
Namespace3\ClassExample::staticMethod();
// OR
\Namespace3\ClassExample::staticMethod();

// Included namespace4.
echo Namespace4\CONST_EXAMPLE . '<br>';
echo Namespace5\CONST_EXAMPLE . '<br>';