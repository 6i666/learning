<?php
/**
 * File contains examples of using namespaces.
 */

namespace Namespace1\Namespace2;

/**
 * Simpe CONST.
 */
const CONST_EXAMPLE = 2;

/**
 * Simpe class.
 */
class ClassExample {
    public static function staticMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
    public function simpleMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
}

/**
 * Simpe function.
 */
function functionExample()
{
    echo __FILE__ . '***' . __FUNCTION__ . '<br>';
}