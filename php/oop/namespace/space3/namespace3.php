<?php
/**
 * File contains examples of using namespaces.
 */

namespace Namespace3;

/**
 * Simpe CONST.
 */
const CONST_EXAMPLE = 3;

/**
 * Simpe class.
 */
class ClassExample {
    public static function staticMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
    public function simpleMethod()
    {
        echo __FILE__ . '***' . __METHOD__ . '<br>';
    }
}

/**
 * Simpe function.
 */
function functionExample()
{
    echo __FILE__ . '***' . __FUNCTION__ . '<br>';
}