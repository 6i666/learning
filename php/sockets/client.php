 <?php
 /**
 * Sockets client part file.
 */

header('Content-Type: text/plain;');
set_time_limit(0);
ob_implicit_flush();
$address = gethostbyname('localhost');
$port = 12345;

try {
    echo "<h2>Клиент</h2>\n";

    // Create TCP/IP socket.
    echo "Создаём TCP/IP сокет.\n";
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        throw new Exception("socket_create(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }

    // Connect to socket.
    echo "Начинает соединение с сокетом.\n";
    $result = socket_connect($socket, $address, $port);
    if ($result === false) {
        throw new Exception('socket_connect() failed: '.socket_strerror(socket_last_error())."\n");
    }

    // Read from socket.
    echo 'Сообщение от сервера: ';
    $out = socket_read($socket, 1024);
    if ($out === false) {
        throw new Exception("socket_read(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }
    echo $out."\n";

    // Write message to socket.
    $msg = "Привет от клиента!";
    if (false === socket_write($socket, $msg, strlen($msg))) {
        throw new Exception("socket_read(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }

    echo 'Ответ сервера: ';
    $out = socket_read($socket, 1024);
    echo $out."\n";

    // Send message to close a socket resource.
    $msg = 'shutdown';
    echo "Говорим серверу: ($msg).\n";
    socket_write($socket, $msg, strlen($msg));
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage();
}
if (isset($socket)) {
    echo 'Закрываем сокет...';
    socket_close($socket);
    echo "OK\n";
}
