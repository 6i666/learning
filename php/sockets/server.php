<?php
/**
 * Socket server part file.
 */

header('Content-Type: text/plain;');
set_time_limit(0);
ob_implicit_flush();
// Get port for service WWW.
// $port = getservbyname('www', 'tcp');
// Port 80 needs root rights so we change it.
$port = 12345;

// Get IP from host.
$address = gethostbyname('localhost');

try {
    echo "<h2>Сервер</h2>\n";

    // Create TCP/IP socket.
    echo "Создаём TCP/IP сокет.\n";
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        throw new Exception("socket_create(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }

    // Bind socket.
    echo "Привязываем имя соккету.\n";
    if (false === socket_bind($socket, $address, $port)) {
        throw new Exception("socket_bind(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }

    // Listen socket.
    echo "Включаем прослушивание соккета.\n";
    if (false === socket_listen($socket)) {
        throw new Exception("socket_listen(): причина: " . socket_strerror(socket_last_error()) . "\n");
    }

    // Accepts a connection on a socket.
    while (true) {
        echo "Принимаем соединение.\n";
        if (($accepted = socket_accept($socket)) === false) {
            throw new Exception("socket_accept(): причина: " . socket_strerror(socket_last_error()) . "\n");
        }

        // Write message to socket.
        $msg = "Привет от сервера!";
        if (false === socket_write($accepted, $msg, strlen($msg))) {
            throw new Exception("socket_read(): причина: " . socket_strerror(socket_last_error()) . "\n");
        }

        while (true) {
            // Read from socket.
            echo 'Сообщение от клиента: ';
            $buf = socket_read($accepted, 1024);
            if ($buf === false) {
            	throw new Exception("socket_read(): причина: " . socket_strerror(socket_last_error()) . "\n");
            }
            echo $buf."\n";

            if (!$buf = trim($buf)) {
            	continue;
            }
            if ($buf == 'shutdown') {
            	socket_close($accepted);
            	break 2;
            }
            echo "Ответ клиенту: ($buf)\n";
            socket_write($accepted, $buf, strlen($buf));
        }
    }
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage();
}

if (isset($socket)) {
    echo "Закрываем сокет...";
    socket_close($socket);
    echo "OK.\n";
}