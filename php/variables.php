<?php
/**
 * File contains examples of using variables.
 */

// Use global variable in function.
$a = 0;
function plus()
{
    global $a;
    $a++;
}
plus();
echo $a . '<br>';

// Use static variable.
function plus2()
{
    static $b = 0;
    echo $b++ . '<br>';
}

plus2();
plus2();


echo '<pre>';
print_r($GLOBALS['a']);